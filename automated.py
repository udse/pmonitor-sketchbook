import subprocess, sys, os, signal, time, serial, argparse

SECONDS_TEST_IS_DELAYED_TO_START                     = 3.0
NUMBER_OF_LINES_TO_READ_BEFORE_RESENDING             = 200

#  Sends a character over the serial port and
#  waits for a different character to be returned
#  on its own line
def SendAndWait(tosend, waitfor):
	resendCount = 0
	ser.write(tosend)
	for line in ser:
		if line.rstrip() == waitfor:
			break		
		resendCount += 1
		if resendCount % NUMBER_OF_LINES_TO_READ_BEFORE_RESENDING == 0:
			print("  Resend")
			ser.write(tosend)
		
#  Waits for sometime and keeps reading values
#  from the serial port inorder to keep the
#  buffer empty
def WaitFor(sometime):
	endTime = sometime + time.time()
	displaySeconds = int(sometime)
	if displaySeconds < 2:
		displaySeconds = 0
	if args.show_wait:
		print("Waiting " + str(sometime) + " s")
	for line in ser:
		if endTime < time.time():
			break
		if args.show_wait and int(endTime-time.time()) < displaySeconds:
			print(str(displaySeconds) + "...")
			displaySeconds = int(endTime-time.time())

#  Records the stream from the arduino program to a csv file
#  and terminates after runTime time.
def LogReadings(stream, runTime, filename):

	print("Started Logging")

	#  File for the raw data
	f = open(filename, 'w')
	#  File for the zeroed out power data
	z = open(filename[:-4] + "_ZEROED.csv", 'w')

	#  Write the header to the files
	f.write("Time,Load Voltage,Current,Power\n")
	z.write("Time,Load Voltage,Current,Power\n")


	startTime = time.time()
	endTime = runTime + startTime
	
	hasTestStarted = False
	displaySecondsBefore = int(SECONDS_TO_START_LOGGING_BEFORE_TEST_STARTS)
	displaySecondsAfter = int(SECONDS_TO_KEEP_LOGGING_AFTER_TEST_FINISHES)
	printedTestCompleted = False

	
	if displaySecondsAfter < 2:
		displaySecondsAfter = 0
	if displaySecondsBefore < 2:
		displaySecondsBefore = 0

	totalEnergy = 0
	lastReadingTime = -1
	lastPowerReading = 0

	if args.show_wait:
		print("Waiting " + str(SECONDS_TO_START_LOGGING_BEFORE_TEST_STARTS) + " s")

	while True:

		#  Reads off one line from stream
		for reading in stream:

			if not hasTestStarted:
				timeRemainingToStart = startTime + SECONDS_TO_START_LOGGING_BEFORE_TEST_STARTS - time.time()
				if timeRemainingToStart <= 0:
					print("Test Started")
					hasTestStarted = True					
				elif int(timeRemainingToStart) < displaySecondsBefore:
					if args.show_wait:
						print(str(displaySecondsBefore) + "...")
						displaySecondsBefore = int(startTime + SECONDS_TO_START_LOGGING_BEFORE_TEST_STARTS - time.time())
			
			elif not printedTestCompleted and endTime-time.time() < SECONDS_TO_KEEP_LOGGING_AFTER_TEST_FINISHES:
				print("Test Completed")
				if args.show_wait:
					print("Waiting " + str(SECONDS_TO_KEEP_LOGGING_AFTER_TEST_FINISHES) + " s")
				printedTestCompleted = True
			elif printedTestCompleted and int(endTime-time.time()) < displaySecondsAfter:
				if args.show_wait:
					print(str(displaySecondsAfter) + "...")
					displaySecondsAfter = int(endTime-time.time())

			#  Time for logging has expired
			if endTime < time.time():
				f.flush()
				f.close()
				z.flush()
				z.close()
				print("Terminated Logging")
				print("Total Power: " + str(totalEnergy / 1000000000))
				return totalEnergy / 1000000000

			#  Using a try block to catch any bad formed
			#  readings from crashing the program
			try:
				#  Remove the newline character at the end
				reading = reading.rstrip()
				split = reading.split(",")

				#  Expecting 3 values to be on one line
				if len(split) < 3:
					continue

				currentReadingTime =   int(split[0])
				volts              = float(split[1])
				amps               = float(split[2])
				watts              = volts * amps

				csv = reading + "," + str(watts) + '\n'
				f.write(csv)

				if watts < 0:
					watts = 0
				
				#  Wait for the second reading to come in before
				#  trying to compute a energy usage
				if lastReadingTime != -1:
					totalEnergy += lastPowerReading * (currentReadingTime - lastReadingTime)
				lastReadingTime = currentReadingTime
				lastPowerReading = watts

				if volts < 0:
					volts = 0
				if amps < 0:
					amps = 0

				csv = str(currentReadingTime) + "," + str(volts) + "," + str(amps) + "," + str(watts) + '\n'
				z.write(csv)

			except:
				print sys.exc_info()[0], ": ", reading
				continue


#-----------------START OF SCRIPT---------------------

ANDROID_SDK_HOME = os.environ.get('ANDROID_SDK_HOME')

parser = argparse.ArgumentParser(description='Process commandline arguments')

parser.add_argument('--reran-path'      , action='store'                   , required=True  , help="Path to RERAN folder containing Translate.jar file")
parser.add_argument('--arduino-path'    , action='store'                   , required=True , help="File path of the arduino's serial port")
parser.add_argument('--file-prefix'     , action='store'                   , required=True , help="Name to prefix each of the test files")
parser.add_argument('--test-count'      , action='store'      , type=int   , required=True , help="The number of tests to be run")
parser.add_argument('--seconds-before'  , action='store'      , type=float , default=1.0   , help="Number of seconds to record values before test begins")
parser.add_argument('--seconds-after'   , action='store'      , type=float , default=0.5   , help="Number of seconds to record values after test ends")
parser.add_argument('--seconds-between' , action='store'      , type=float , default=2.0   , help="Number of seconds to wait between test runs")
parser.add_argument('--adb-path'        , action='store'                   , default=None  , help="Path to adb executable. Required if 'ANDROID_SDK_HOME' is not available as an environment variable")
parser.add_argument('--serial-rate'     , action='store'      , type=int   , default=115200, help="The data rate of the arduino's serial port", choices=[300, 600, 1200, 2400, 4800, 9600, 14400, 19200, 28800, 38400, 57600, 115200])
parser.add_argument('--show-wait'       , action='store_true'                              , help="Use to show the times spent waiting between operations")
args = parser.parse_args()

SECONDS_TO_START_LOGGING_BEFORE_TEST_STARTS = args.seconds_before
SECONDS_TO_KEEP_LOGGING_AFTER_TEST_FINISHES = args.seconds_after
SECONDS_TO_WAIT_BETWEEN_TESTS               = args.seconds_between

SECONDS_TEST_IS_DELAYED_TO_START += SECONDS_TO_START_LOGGING_BEFORE_TEST_STARTS

if args.adb_path == None:
	if ANDROID_SDK_HOME == None:
		print(sys.argv[0] + ": error: argument --adb-path is required since the environment variable 'ANDROID_SDK_HOME' is not available")
		sys.exit(1)
else:
	ANDROID_SDK_HOME = args.adb_path

ADB_PATH = os.path.join(os.path.join(ANDROID_SDK_HOME, "platform-tools"), "adb")
RERAN_PATH = os.path.join(args.reran_path, "translate.jar")

if not os.path.exists(ADB_PATH):
	print("error: cannot find adb executable")
	sys.exit(1)

if not os.path.exists(RERAN_PATH):
	print("error: cannot find 'translate.jar' in RERAN path")
	sys.exit(1)

ARGUMENTS_TO_START_RECORDING    = [ADB_PATH, "shell", "getevent", "-tt"]
ARGUMENTS_TO_START_TRANSLATING  = ["java", "-jar", RERAN_PATH, "recordedEvents.txt", "translatedEvents.txt"]
ARGUMENTS_TO_UPLOAD_RECORDING   = [ADB_PATH, "push" , "translatedEvents.txt", "/data/local/tmp"]
ARGUMENTS_TO_START_RERAN	= [ADB_PATH, "shell", "/data/local/tmp/replay.exe /data/local/tmp/translatedEvents.txt " + str(int(SECONDS_TEST_IS_DELAYED_TO_START)) + " &"]

#  Open the serial port to the arduino
ser = serial.Serial(args.arduino_path, args.serial_rate)

print("Waiting for arduino to start up")
#  Wait for arduino to turn on and make sure the USB cord is turned on
SendAndWait('E', 'ON')
WaitFor(2.0)

#  If a RERAN recording is not in the current folder then start recording one
if not os.path.exists('./recordedEvents.txt'):
	with open('recordedEvents.txt','w') as recFile:
		print("Starting recording")
		print("    Please enter an input sequence into the phone(press ENTER when done)")
		record = subprocess.Popen(ARGUMENTS_TO_START_RECORDING, stdout=recFile)
		raw_input()
		record.send_signal(signal.SIGKILL)

print("Translating recording")
#  Translate recording into RERAN format
subprocess.call(ARGUMENTS_TO_START_TRANSLATING)


print("Uploading input sequence")
#  Upload the RERAN recording to the phone
subprocess.call(ARGUMENTS_TO_UPLOAD_RECORDING)

#  Calculate the runtime of the RERAN recording just uploaded to the phone
runTime = 0
with open('./translatedEvents.txt', 'r') as file:
	file.readline()
	for line in file:
		split = line.rstrip().split(',')
		if len(split) == 1:
			runTime += int(split[0])
runTime /= 1000000000
print("Test run will last " + str(runTime) + " s")

print("\n\n------Starting Tests-------\n")

energyResults = []

#  Loops over the range of the number of tests that are to be run
for index in range(1, 1+args.test_count):
	filename = args.file_prefix + str(index) + ".csv"

	if os.path.exists(filename):
		print("File already exists: " + filename + "   Skipping...")
		energyResults.append("N/A")
		continue

	print("Starting: " + filename)

	with open(os.devnull, 'w') as tempf:

		#  Start the test on the phone
		while(subprocess.Popen(ARGUMENTS_TO_START_RERAN, stdout=subprocess.PIPE).stdout.readline().rstrip() != "STARTED"):
			print("USB not cut")
		phoneStartTime = time.time()

		print("Started Replay")

		print("Waiting for USB to turn off");
		#  Tell the arduino to cut the power and wait for it to reply
		SendAndWait('D', 'OFF')
		
		timeToStartLogging = (SECONDS_TEST_IS_DELAYED_TO_START + phoneStartTime - SECONDS_TO_START_LOGGING_BEFORE_TEST_STARTS) - time.time()
		if timeToStartLogging < 0:
			print("TEST STARTED BEFORE USB TURNED OFF")		
		WaitFor(timeToStartLogging)
		
		logRunTime = runTime + SECONDS_TO_KEEP_LOGGING_AFTER_TEST_FINISHES + SECONDS_TO_START_LOGGING_BEFORE_TEST_STARTS
		result = LogReadings(ser, logRunTime, filename)

		energyResults.append(str(result))
	
		enableUSBTime = time.time()
		print("\nWaiting for USB to turn on");
		#  Tell the arduino to turn on the power and wait for it to reply
		SendAndWait('E', 'ON')

	if index != args.test_count:
		print("Preparing next test")
		WaitFor(SECONDS_TO_WAIT_BETWEEN_TESTS - (time.time()-enableUSBTime))

	print('')

print("Tests completed")
for res in energyResults:
	print(res)
ser.close()
