#!/usr/bin/env python

import os, serial, time, threading, subprocess

DEVNULL = open(os.devnull, 'wb')

class ArduinoLogger(object):
	def __init__(self, port, filename):
		self.serial = serial.Serial(port, 115200, timeout = 2)
		self.thread = threading.Thread(target = self.__read)
		self.start_event = threading.Event()
		self.stop_event = threading.Event()
		self.file = open(filename, "w")

		# Discard old / wrong data until we know the arduino finished resetting
		#count = 0
		#while self.serial.readline().strip() != "-=+* ARDUINO INITIALIZED *+=-":
		#	count += 1
		#	if count % 300 == 0:
		#		self.serial.close()
		#		self.serial = serial.Serial(port, 115200, timeout = 2)
		#		print("\tWaiting for {0}".format(port))
		#	line = self.serial.readline().strip()

		self.thread.start()

	def __read(self):
		# Discard data until the execution of interest has started.
		while not self.start_event.is_set():
			self.serial.readline()

		while not self.stop_event.is_set():
			fields = self.serial.readline().strip().split(",")

			assert len(fields) == 3

			time = int(fields[0])
			voltage = float(fields[1])
			current_mA = float(fields[2])

			power_mW = voltage * current_mA if current_mA > 0 else 0
			self.file.write("{0},{1}\n".format(time, power_mW))
			
	def start(self):
		self.start_event.set()

	def stop(self):
		self.stop_event.set()
		self.thread.join()
		self.serial.close()
		self.file.close()

def main():
	import argparse
	
	parser = argparse.ArgumentParser()

	parser.add_argument('replay',
                            help     = "Replay to execute")

	parser.add_argument('--arduino',
                            dest     = 'arduinos',
        	            action   = 'append',
        	            default  = ["/dev/ttyACM0", "/dev/ttyACM1"],
        	            help     = "File path of an arduino serial port")

	parser.add_argument('-n',
        	            action   = 'store',
	                    type     = int,
	                    default  = 1,
	                    help     = "The number of trials to run")

	parser.add_argument('--android-sdk',
	                    action   = 'store',
	                    default  = os.environ.get('ANDROID_SDK_HOME'),
	                    help     = "Path to the adb executable")

	parser.add_argument('--reran',
			    action   = 'store',
			    required = True,
			    help     = "Path to RERAN folder containing Translate.jar file")

	parser.add_argument('--file-name',
			    action   = 'store',
			    required = True,
			    help     = "Name for the recording files")

	args = parser.parse_args()

	adb = os.path.join(args.android_sdk, "platform-tools", "adb")
	reran = os.path.join(args.reran, "translate.jar")
	filename = args.file_name

	print("----Translating recording")
	subprocess.call(["java", "-jar", reran, args.replay, "translatedEvents.txt"], stdout = DEVNULL, stderr = subprocess.STDOUT)
	
	print("----Pushing recording to device")
	subprocess.call([adb, "push" , "translatedEvents.txt", "/data/local/tmp/replay"], stdout = DEVNULL, stderr = subprocess.STDOUT)
	
	for trial in range(0, args.n):
		logs = ["{0}-{1}-{2}.log".format(filename, trial, os.path.basename(arduino)) for arduino in args.arduinos]
		logcat = "{0}-{1}-LOGCAT.log".format(filename, trial)
		reran = "{0}-{1}-RERAN.log".format(filename, trial)

		loggers = [ArduinoLogger(path, log) for path, log in zip(args.arduinos, logs)]
		time.sleep(3)

		print("----Running trial {0}".format(trial))

		with open(logcat, 'w') as logcat_log, open(reran, 'w') as reran_log:
			subprocess.call([adb, "logcat", "-c"])
			logcat = subprocess.Popen([adb, "logcat"], stdout = logcat_log,  stderr = subprocess.STDOUT)

			for logger in loggers:
				logger.start()

			subprocess.call([adb, "shell", "/data/local/tmp/replay.exe", "/data/local/tmp/replay", "0"], stdout = reran_log, stderr = subprocess.STDOUT)

			for logger in loggers:
				logger.stop()

			logcat.kill()

if __name__ == '__main__':
    main()
